const { decode } = require('ini')
const { readFileSync, existsSync } = require('fs')
const { normalize, join } = require('path')
const isBoolean = require('lodash.isboolean')
const isString = require('lodash.isstring')
const isNumber = require('lodash.isnumber')
const trim = require('lodash.trim')

// Convert a value to its logical boolean equivalent
function convertToBoolean (value) {
  if (isBoolean(value)) return value
  if (isString(value)) return (value.toLowerCase().trim() === 'true' || value.toLowerCase().trim() === 't')
  if (isNumber(value)) return (value === 1)
  return value
}

// If string includes a dot, then we'll parseFloat
function convertToNumber (value) {
  return Number.isInteger(value)
    ? parseInt(value, 10)
    : parseFloat(value)
}

// Convert a value from a comma separated value to an array
function convertToArray (value) {
  if (Array.isArray(value)) return value

  // Comma Separated Values, split and trim
  if (isString(value)) return (value.split(',').map(trim))

  return value
}

function getIniValues (cwd, filename = '.env') {
  // Optional file that is exluded from git for holding env vars
  const iniFileName = normalize(join(cwd, filename))
  const iniFileExists = existsSync(iniFileName)

  return iniFileExists ? decode(readFileSync(iniFileName, 'utf-8')) : {}
}

Object.assign(exports, {
  convertToBoolean,
  convertToArray,
  convertToNumber,
  getIniValues,
})
