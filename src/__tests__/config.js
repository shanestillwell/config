const Config = require('../')

process.env.BLUE = 'frog'
process.env.YELLOW = 'false'
process.env.RED = '5'
process.env.PINK = 'one,two, three'
process.env.GREEN = '3.5'

it('should accept defaults', () => {
  const config = Config({
    CHEESE: 'GROMIT',
    HERBIE: false,
    LARRY: 5,
  })
  expect(config.get('CHEESE')).toBe('GROMIT')
  expect(config.get('HERBIE')).toBe(false)
  expect(config.get('LARRY')).toBe(5)
})

it('should cast environment variables and override', () => {
  const config = Config({
    BLUE: 'toad',
    YELLOW: true,
    RED: 3,
    PINK: [],
    GREEN: 2.2,
  })
  expect(config.get('BLUE')).toBe('frog')
  expect(config.get('YELLOW')).toBe(false)
  expect(config.get('RED')).toBe(5)
  expect(Number.isInteger(config.get('RED'))).toBe(true)
  expect(config.get('PINK')).toEqual(expect.arrayContaining(['one', 'two', 'three']))
  expect(config.get('GREEN')).toBe(3.5)
  expect(Number.isInteger(config.get('GREEN'))).toBe(false)
})

it('should use shortcuts', () => {
  const config = Config({
    BLUE: 'toad',
    YELLOW: true,
    RED: 3,
    PINK: [],
  })
  expect(config.BLUE).toBe('frog')
  expect(config.YELLOW).toBe(false)
  expect(config.RED).toBe(5)
  expect(config.PINK).toEqual(expect.arrayContaining(['one', 'two', 'three']))
})

it('should read from .env file', () => {
  const config = Config({
    FOO: 'One',
    NV: 2,
    DAYS: [],
  })
  expect(config.FOO).toBe('Web')
  expect(config.NV).toBe(1)
  expect(config.DAYS).toEqual(expect.arrayContaining(['one', 'two', 'three']))
})
