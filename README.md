# Config

> A configuration manager utilizing [nconf](https://github.com/indexzero/nconf). The claim to fame: casts the incoming process.env strings to their corrisponding default types

## Install

```
yarn install @northernv/config
```

## Usage

```javascript
// config.js
const Config = require('@northernv/config')

const defaults = {
  WWW_URL: 'https://www.example.com',
  IS_AWESOME: true,
  SCORE: 100,
  COLORS: ['red', 'green', 'blue'],
  RATE: 3.5,
}

module.exports = Config(defaults)
```

```javascript
// index.js
const config = require('./config')

// Is cast to a boolean, because the default is a Boolean
config.get('IS_AWESOME') // => true

// Is cast to integer because the default is a integer
config.get('SCORE') // => 100

// Is cast to a float because the default is a float
config.get('RATE') // => 3.5
```

## Sugar

```javascript
// Values can be accessed directly

config.IS_AWESOME // => true

as opposed to the longer way of 

config.get('IS_AWESOME') // => true
```


## Types

* Strings
* Booleans
* Integers
* Floats
* CSV => Array

## NOTES

* Precedence
  1. Environment Variables trump all
  1. `.env` in the root directory takes second
  1. Passed in `defaults` object

* Casting

  All values should have a default in the type they expect to be. For example, if you use a variable string `WWW_URL = 'https://www.example.com'`. You should declare a default value so it knows how to handle incoming environment variables.

* Never store sensitive information in code.
  Instead just use
  ```
  const default = {
    DB_PASSWORD: 'REDACTED'
  }
  ```

* `.env` was deliberately added to git for testing
